// ввод только положительных цифр в поля input настройки калькулятора
$.fn.inputFilter = function (inputFilter) {
  return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
      if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
  });
};
$(".content__calc-setting-input").inputFilter(function (value) {
  return /^\d*$/.test(value);
});


//Скрипт для мобильного меню сайта
$(document).ready( function() {
    $(".toggle-icon").click(function () {
    $('#nav-container').toggleClass("pushed");
    });
    $("#nav-container").on("click", function () {
        $(this).next().slideToggle(600);
    });
});


//Скрипт для кнопок футера
$(".footer__button-menu").on("click", function () {
    $(this).toggleClass("footer__button-menu_toggle");
    $(this).toggleClass("header__mobile-menu_button_toggle");
    $(this).next().slideToggle(600);
});


//Скрипт для кнопки меню на мобильных устройствах
$(".header__mobile-menu_button").on("click", function () {
    $(this).toggleClass("header__button-menu_toggle");
    $(this).toggleClass("header__mobile-menu_button_toggle");
    $(this).next().slideToggle(600);
});


// открытие модального окна для изменения информации о пользователе
$(document).on("click", '.content__table-users_edit', function (event) {
    event.preventDefault();
    $idUserEdit = $(this).attr('data-id');
    $loginUserEdit = $(this).attr('data-login-user');

    $('.modal__window h2').html("Обновите пользователя:");
    $('#modal__window_login-user').val($loginUserEdit);

    $('#modal__window_overlay').fadeIn(600, function () {
        $('#modal__window')
            .css('display', 'block')
            .animate({
                opacity: 1,
                top: '-6vh'
            }, 200);
    });
    $('.editUser__text-successfully span').html('');
});

// закрытие модального окна для изменения информации о пользователе
$('.modal__window_close-all').click(function (event) { // лoвим клик пo крестику или пoдлoжке
    event.preventDefault();
    $('#modal__window')
        .animate({
            opacity: 0,
            top: '300px'
        }, 200, function () {
            $(this).css('display', 'none'); // делaем ему display: none;
            $('#modal__window_overlay').fadeOut(400); // скрывaем пoдлoжку
        });
});

//добавление нового пользователя
$(document).on("click", '#user__add', function (event) {
    event.preventDefault();

    let idUser = $(this).attr('data-id'),
        login = $('#user__login'),
        login_value = $('#user__login').val(),
        password = $('#user__passwd'),
        password_value = $('#user__passwd').val();

        $('.adduser__text-successfully span').html('');
        $(login).removeClass('error-box');
        $(password).removeClass('error-box');

        if (login_value == "" || password_value == "") {
            if (login_value == "") $(login).addClass('error-box');
            if (password_value.length == "") $(password).addClass('error-box');
        return;
        }

        if (password_value.length < '5') {
            $(password).addClass('error-box');
            $('.adduser__text-successfully span').text('Минимальная длина пароля 5 символов!');
        }

        if (login_value != '' && password_value.length >= '5') {
            $.ajax({
                type: "POST",
                url: "../../app/controllers/handler.php",
                data: {
                    usLogin:login_value,
                    usPassword:password_value,
                    command:"addUser",
                    usId:idUser
                }
                }).done(function(data) {
                    let user = JSON.parse(data);

                    $('.adduser__text-successfully span').text(user.alreadyExists).fadeIn(0);
                    $('.content__table-users_table tbody').append(user.html);
                    $('.adduser__text-successfully span').text(user.exist).delay(2000).fadeOut(600);
                });
        $('#user__login').val('');
        $('#user__passwd').val('');
        }
});


// Удаление пользователей
$(document).on("click", '.content__table-users_delete', function(event) {
    event.preventDefault();

    let idUser = $(this).attr('data-id');
    $.ajax({
        type: "POST",
        url: "../../app/controllers/handler.php",
        data: {
            usIdDelete:idUser,
            command:"deleteUser"
        },
    }).done(function(data) {
    });
    $(this).closest("tr").fadeOut(555);
})

//Изменение информации о пользователе
$(document).on("click", '#modal__window_button_apply', function(event) {
    event.preventDefault();

    let idUser = $idUserEdit, // тянется по нажатию на кнопку "Изменить пользователя"
        login = $('#modal__window_login-user'),
        login_value = $('#modal__window_login-user').val(),
        password = $('#modal__window_passwd-user'),
        password_value = $('#modal__window_passwd-user').val();

    $(login).removeClass('error-box');
    $(password).removeClass('error-box');

    if (login_value == "") {
        if (login_value == "") $(login).addClass('error-box');
    return;
    }

    if (login_value != '') {
        $.ajax({
            type: "POST",
            url: "../../app/controllers/handler.php",
            data: {
                editLogin:login_value,
                editPassword:password_value,
                editId:idUser,
                command:"editUser"
            }
        }).done(function(data) {
            let editUser = JSON.parse(data);
            if (editUser.bool) {
                $('#userLogin'+idUser).html(login_value);
                $('.editUser__text-successfully span').html(editUser.successfully);

                function closeEditUser() {

                $('#modal__window').animate({opacity: 0, top: '100px'}, 300, function () {
                    $(this).css('display', 'none'); // делaем ему display: none;
                    $('#modal__window_overlay').fadeOut(400); // скрывaем пoдлoжку
                });

                $(login).val('');
                $(password).val('');
                }

                setTimeout(closeEditUser, 600);
                } else {
                $('.editUser__text-successfully span').html(editUser.alreadyExists);
            }
        });
    }
});

//открытие модального окна для добавления цвета
$(document).on("click", '.click__add-color', function (event) {
    event.preventDefault();

    $('#modal__window_add-color-overlay').fadeIn(600, function () {
        $('#modal__window_add-color')
            .css('display', 'block')
            .animate({
                opacity: 1,
                top: '-6vh'
            }, 200);
    });
    $('.addColor__text-successfully span').html('');
    $('#modal__window_add-color-name').val('');
});

// закрытие модального окна для добавления цвета
$(document).on("click", '.modal__window_add-color-close-all', function () { // лoвим клик пo крестику или пoдлoжке
    event.preventDefault();
    $('#modal__window_add-color')
        .animate({
            opacity: 0,
            top: '300px'
        }, 200, function () {
            $(this).css('display', 'none'); // делaем ему display: none;
            $('#modal__window_add-color-overlay').fadeOut(400); // скрывaем пoдлoжку
        });
});

//добавление нового цвета
$(document).on("click", '#modal__window_add-color-button_apply', function (event) {
    event.preventDefault();

    let color = $('#modal__window_add-color-name'),
        color_value = $('#modal__window_add-color-name').val();

        // Ввод названий цвета только с заглавной буквы
        new_text = color_value.charAt(0).toUpperCase() + color_value.substr(1);
        $(this).val(new_text);
    let color_value_upper = this.value;

    // Ввод только букв
    $(document).on("keyup", '#modal__window_add-color-name', function() {
        this.value=this.value.replace(/[^a-zA-Zа-яА-Я]/g,'');
    });

    $('.addColor__text-successfully span').text('');
    $(color).removeClass('error-box');

    if (color_value_upper == "") {
        if (color_value_upper.length == "") $(color).addClass('error-box');
    return;
    }

    if (color_value_upper.length < '4') {
        $(color).addClass('error-box');
        $('.addColor__text-successfully span').text('Минимальная длина цвета 4 символа!');
    } else {
    $.ajax({
        type: "POST",
        url: "../../app/controllers/handler.php",
        data: {
            colorName:color_value_upper,
            command:"addColor"
        }
        }).done(function(data) {
            let colorJson = JSON.parse(data);
                $('.content__colors-from-database').append(colorJson.html);
                $('.addColor__text-successfully span').text(colorJson.exists);

                if (colorJson.flag == true) {
                    
                    function closeAddColor() {

                    $('#modal__window_add-color').animate({opacity: 0, top: '100px'}, 300, function () {
                        $(this).css('display', 'none'); // делaем ему display: none;
                        $('#modal__window_add-color-overlay').fadeOut(400); // скрывaем пoдлoжку
                    });
                    $(this).val('');
                    }

                    setTimeout(closeAddColor, 600);
                }
        });
    }
});

//Удаление цвета
$(document).on("click", '.content__calc-setting_delete', function(event) {
    event.preventDefault();

    let idColor = $(this).attr('data-id-delete');
    $.ajax({
        type: "POST",
        url: "../../app/controllers/handler.php",
        data: {
            colorIdDelete:idColor,
            command:"deleteColor"
        }
    }).done(function(data) {
        console.log(data);
    });
    $(this).closest(".content__calc-setting_color-options").fadeOut(555);
})

// Изменение настроек калькулятора
$(document).on("click", '#saving-of-chenges', function(event) {
    event.preventDefault();

    let textureMatt = $('#cost__texture_matt'),
        textureMatt_value = $('#cost__texture_matt').val(),
        pipes = $('#cost__pipes'),
        pipes_value = $('#cost__pipes').val(),
        textureGlossy = $('#cost__texture_glossy'),
        textureGlossy_value = $('#cost__texture_glossy').val(),
        chandeliers = $('#cost__chandeliers'),
        chandeliers_value = $('#cost__chandeliers').val(),
        square = $('#cost__square'),
        square_value = $('#cost__square').val(),
        light = $('#cost__light'),
        light_value = $('#cost__light').val(),
        corner = $('#cost__corner'),
        corner_value = $('#cost__corner').val();

        if (textureMatt_value > 0 && pipes_value > 0 && textureGlossy_value > 0 && chandeliers_value > 0 && square_value > 0 && light_value > 0 && corner_value > 0) {
        $.ajax({
            type: "POST",
            url: "../../app/controllers/handler.php",
            data: {
                textureMatt:textureMatt_value,
                pipes:pipes_value,
                textureGlossy:textureGlossy_value,
                chandeliers:chandeliers_value,
                square:square_value,
                light:light_value,
                corner:corner_value,
                command:"settingCalculator"
            }
        }).done(function(data) {
            alert(data);
            $(textureMatt).val('');
            $(pipes).val('');
            $(textureGlossy).val('');
            $(square).val('');
            $(light).val('');
            $(corner).val('');
            $(chandeliers).val('');
        });
        } else {
            alert('Пожалуйста, заполните все поля!');
        }
});

// Удаление заявок от пользователей в бд и алминке
$(document).on("click", '.content__table-applications_delete', function(event) {
    event.preventDefault();

    let idRequest = $(this).attr('data-id');

    $.ajax({
        type: "POST",
        url: "../../app/controllers/handler.php",
        data: {
            requestIdDelete:idRequest,
            command:"deleteRequest"
        }
    }).done(function(data) {
        console.log(data);
    });
    $(this).closest("tr").fadeOut(555);
})