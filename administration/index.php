<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/app/class/users.class.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/app/class/colors.class.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/app/class/calculator.setting.class.php");
session_start();
if (!isset($_SESSION['userLoged']) or ($_SESSION['userLoged'] != 'Loged'))
{
	header('Location: /check_in/index.php');
    exit;
}
?>

<!DOCTYPE html>
<html lang="ru">

<head>
    <link href="faviconka_ru_1133.png" rel="shortcut icon" type="image/png">
    <meta charset="UTF-8">
    <title>Administration panel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="css/reset.css" rel="stylesheet" />

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <script src="js/jquery3.3.1.js" defer></script>
    <script src="js/script.js" defer></script>
    <link href="css/style.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/adaptive.css" />
</head>
<body>

    <!--—BEGIN HEADER -->
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-3 col-lg-2 col-md-2 header__content-left">
                    <a href="#" class="header__logo">
                        <img src="images/header-logo.png" alt="Юкон"></a>
                </div>
                <div class="col-4 col-lg-4 col-md-4 header__column_adress">
                    <address class="header__top-text-column">
                        <div><i class="fas fa-map-marker-alt"></i> г. Ростов-на-Дону, Шаумяна, 73</div>
                    </address>
                    <address class="header__text">
                        <div>г. Волгодонск, ул. Энтузиастов, 13</div>
                    </address>
                </div>
                <div class="col-2 col-lg-3 col-md-3 header__column_tel">
                    <div class="header__number-phone">
                        <a href="tel:+78632298182" class="header__top-text-column"><i class="fas fa-phone-volume"></i> +7 (863) 229-81-82</a>
                        <a href="tel:+78639247979" class="header__text">+7 (863) 924-79-79</a>
                    </div>
                </div>
                <div class="col-3 col-lg-3 col-md-3 header__column_calc">
                    <div class="header__calc-online">
                        <a href="#">
                            <div class="header__vertical">
                                <i class="fas fa-calculator"></i>
                                <div class="header__calc-text">Калькулятор<span class="header__calc-text_none"> онлайн</span></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div id="nav-container" class="header__nav-container">
            <div class="toggle-icon">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>
        </div>
        <nav id="header__mobile-menu" class="header__mobile-menu">
            <menu>
                <li><a href="#">о нас</a></li>
                <li><a href="#">каталог</a></li>
                <li><a href="#">услуги</a></li>
                <li><a href="#">цены</a></li>
                <li><a href="#">конструкции</a></li>
                <li><a href="#">акции</a></li>
                <li><a href="#">наши работы</a></li>
                <li><a href="#">контакты</a></li>
            </menu>
            <div class="header__mobile-menu_button">Наши адреса:</div>
            <ul class="header__mobile-menu_address">
                <li>
                    <address> г. Ростов-на-Дону, Шаумяна, 73</address><br>
                    <a href="tel:+78632298182">+7 (863) 229-81-82</a>
                </li>
                <li>
                    <address> г. Волгодонск, ул. Энтузиастов, 13</address><br>
                    <a href="tel:+78639247979">+7 (863) 924-79-79</a>
                </li>
            </ul>
            <div class="header__calc-online header__mobile-menu_calc">
                <a href="#">
                    <div class="vertical">
                        <i class="fas fa-calculator"></i>
                        <div class="header__calc-text">Калькулятор<p class="header__calc-text_none"> онлайн</p>
                        </div>
                    </div>
                </a>
            </div>
        </nav>
    </header>
    <!--—END HEADER -->

    <!--—BEGIN Content -->
    <div class="content">
        <div class="container">
            <div class="content__users">
                <h2 class="content__zag">Пользователи</h2>
                <table class="content__table-users_table">
                    <tr class="">
                        <th>ID</th>
                        <th>Логин пользователя</th>
                        <th>Действие</th>
                    </tr>

                    <!-- Вывод информации о пользователях из БД -->
                    <?php
                        $userList = User::GetAllUsers();
				        foreach ($userList as $key => $value)
                        {
                    ?>
                        <tr>
                            <td>
                                <?=$value['id_user']?>
                            </td>
                            <td id="userLogin<?=$value['id_user']?>">
                                <?=$value['login']?>
                            </td>
                            <td class="content__table-users_last-column">
                                <a href="#" class="content__table-users_edit" data-id="<?= $value['id_user']?>" data-login-user="<?=$value['login']?>"></a>
                                <a href="#" class="content__table-users_delete" data-id="<?= $value['id_user']?>">&#215;</a>
                            </td>
                        </tr>
                    <?php
				        }
				    ?>
                </table>
                <!-- Конец вывода информации о пользователях из БД -->

                <!-- Добавления нового пользователя -->
                <p>Добавление нового пользователя</p>
                <form action="" method="post" id="content__users_add-users">
                    <div class="row">
                        <div class="col-4">
                            <label for="user__login">Логин:</label>
                            <input type="text" id="user__login" name="login" class="content__input content__input-addUser_login" placeholder="Логин пользователя" minlength="3">
                            <label for="user__passwd">Пароль:</label>
                            <input type="password" name="password" id="user__passwd" class="content__input content__input-addUser_passwd" placeholder="Пароль пользователя" minlength="5">
                            <button type="submit" id="user__add" class="content__users_add">Добавить пользователя</button>
                            <div class="adduser__text-successfully"><span></span></div>
                        </div>
                    </div>
                </form>
                <!-- Конец добавления нового пользователя -->

                <!--—BEGIN modal-window on edit user -->
                <div id="modal__window" class="modal__window">
                    <span id="modal__window_close" class="modal__window_close modal__window_close-all">
                        <span class="modal__window_close-wrapper modal__window_close-all">
                            <span class="modal__window_line1 modal__window_close-all"></span>
                            <span class="modal__window_line2 modal__window_close-all"></span>
                        </span>
                    </span>
                    <form action="#" class="edit-user" method="post">
                        <h2></h2>
                        <label for="modal__window_login-user">Логин пользователя:</label>
                        <input type="text" id="modal__window_login-user" class="modal__window_login-user">
                        <label for="modal__window_passwd-user">Пароль пользователя:</label>
                        <input type="password" id="modal__window_passwd-user" class="modal__window_passwd-user">
                        <div class="editUser__text-successfully"><span></span></div>
                        <button type="submit" id="modal__window_button_apply" class="modal__window_button_apply">Применить</button>
                        <button class="modal__window_close-all">Отмена</button>
                    </form>
                </div>
                <div id="modal__window_overlay" class="modal__window_overlay modal__window_close-all"></div>
                <!--— END modal-window on edit user -->

	            <!-- Начало вывода настроек калькулятора из бд -->
				<?php
					$calcSettingList = CalculatorSettings::getCalcSetting();

				echo "</div>
	            <div class='content__calc-setting'>
	                <h2 class='content__zag'>Настройки калькулятора</h2>
	                <form action='#' method='post' id='content__calc-setting_form'>
	                    <div class='row'>
	                        <div class='col-4 order-1 order-md-3'>
	                            <label for='cost__texture_matt'>Цена за матовую фактуру:</label>
	                            <input type='text' id='cost__texture_matt' class='content__input content__calc-setting-input' value='".$calcSettingList['cost_matt_texture']."' placeholder='0'>
	                            <label for='cost__pipes'>Цена за трубу:</label>
	                            <input type='text' id='cost__pipes' class='content__input content__calc-setting-input' value='".$calcSettingList['cost_pipe']."' placeholder='0'>
	                        </div>
	                        <div class='col-4 order-2 order-md-2'>
	                            <label for='cost__texture_glossy'>Цена за глянцевую фактуру:</label>
	                            <input type='text' id='cost__texture_glossy' class='content__input content__calc-setting-input' value='".$calcSettingList['cost_glossy_texture']."' placeholder='0'>
	                            <label for='cost__chandeliers'>Цена за люстру:</label>
	                            <input type='text' id='cost__chandeliers' class='content__input content__calc-setting-input' value='".$calcSettingList['cost_chandelier']."' placeholder='0'>
	                        </div>
	                        <div class='col-4 order-3 order-md-1'>
	                            <label for='cost__square'>Цена за кв. м. потолка:</label>
	                            <input type='text' id='cost__square' class='content__input content__calc-setting-input' value='".$calcSettingList['cost_square']."' placeholder='0'>
	                            <label for='cost__light'>Цена за светильник:</label>
	                            <input type='text' id='cost__light' class='content__input content__calc-setting-input' value='".$calcSettingList['cost_light']."' placeholder='0'>
	                            <label for='cost__corner'>Цена за угол:</label>
	                            <input type='text' id='cost__corner' class='content__input content__calc-setting-input' value='".$calcSettingList['cost_corner']."' placeholder='0'>";
				?>
				<!-- Конец вывода настроек калькулятора из бд -->

                            <p>Варианты цветов:</p>
                            <div class='content__colors-from-database'>

                            <!-- Вывод цветов из БД -->
                            <?php
                                $colorLists = Color::getAllColors();
                                foreach ($colorLists as $key => $value) {
                            ?>
                                <div class="content__calc-setting_color-options">
                                    <?=$value?>
                                    <a href="#" class="content__calc-setting_delete" data-id-delete="<?=$key?>">&#215;</a>
                                </div>
                            <?php
                                }
                            ?>
                            <!-- конец вывода цветов из БД -->

                            </div>

                            <div class="content__add-new-color">
                                <span class="click__add-color">Добавить новый цвет</span>
                                <a href="#" class="content__calc-setting_add-color click__add-color">+</a>
                            </div>

                             <!-- BEGIN modal-window on add color -->
                            <div id="modal__window_add-color" class="modal__window_add-color">
                                <span id="modal__window_add-color-close" class="modal__window_add-color-close modal__window_add-color-close-all">
                                    <span class="modal__window_add-color-close-wrapper modal__window_add-color-close-all">
                                        <span class="modal__window_add-color_line1 modal__window_add-color-close-all"></span>
                                        <span class="modal__window_add-color_line2 modal__window_add-color-close-all"></span>
                                    </span>
                                </span>
                                <form action="#" class="form_add-color" method="post">
                                    <label for="modal__window_add-color-name" class="modal__window_add-color-label">Введите новый цвет:</label>
                                    <input type="text" id="modal__window_add-color-name" class="modal__window_add-color-name" onkeyup="this.value=this.value.replace(/[^a-zA-Zа-яА-Я]/g,'');">
                                    <div class="addColor__text-successfully"><span></span></div>
                                    <button type="submit" id="modal__window_add-color-button_apply" class="modal__window_add-color-button_apply">Применить</button>
                                </form>
                            </div>
                            <div id="modal__window_add-color-overlay" class="modal__window_add-color-overlay modal__window_add-color-close-all"></div>
                            <!-- END modal-window on add color -->

                            <button tyoe="submit" id="saving-of-chenges" class="saving-of-chenges">Cохранить изменения</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="content__applications">
                <h2 class="content__zag">Заявки</h2>
                <table>
                    <tr>
                        <th>ID</th>
                        <th>Телефон</th>
                        <th>Дата рождения</th>
                        <th>Город доставки</th>
                        <th>Текст заявки</th>
                        <th>Дата заявки</th>
                        <th>IP</th>
                        <th>Действие</th>
                    </tr>

                    <!-- Начало вывода заявок из бд -->
                    <?php
                        $requestList = User::getAllRequest();
                        foreach ($requestList as $key => $value)
                        {
                    ?>
                        <tr>
                            <td><?=$value['id_request']?></td>
                            <td><?=$value['phone']?></td>
                            <td><?=$value['date_of_birth']?></td>
                            <td><?=$value['delivery_city']?></td>
                            <td><?=$value['text_request']?></td>
                            <td><?=$value['application_date']?></td>
                            <td><?=$value['ip_user']?></td>
                            <td><a href="#" class="content__table-applications_delete" data-id="<?=$value['id_request']?>">&#215;</a></td>
                        </tr>
                    <?php
                        }
                    ?>
                    <!-- Конец вывода заявок из бд -->

                </table>
            </div>
        </div>
    </div>
    <!--—END Content -->

    <!--—BEGIN footer -->
    <footer class="footer">
        <div class="footer__top">
            <div class="container">
                <div class="row">
                    <div class="col-2 col-lg-2 footer__col_1-element footer__mobile-menu_wraper">
                        <h4 class="footer__button-menu footer__button-menu_top-element">Компания</h4>
                        <ul class="footer__mobile-menu_wraper-menuli">
                            <li><a href="#">О нас</a></li>
                            <li><a href="#">Цены</a></li>
                            <li><a href="#">Акции</a></li>
                            <li><a href="#">Наши работы</a></li>
                            <li><a href="#">Гарантия</a></li>
                            <li><a href="#">Калькулятор</a></li>
                        </ul>
                        <div class="footer__button-menu_check-mark"></div>
                    </div>
                    <div class="col-3 col-lg-3 footer__col_2-element footer__mobile-menu_wraper">
                        <h4 class="footer__button-menu">Каталог потолков</h4>
                        <ul class="footer__mobile-menu_wraper-menuli">
                            <li><a href="#">Глянцевые</a></li>
                            <li><a href="#">Матовые</a></li>
                            <li><a href="#">Сатиновые</a></li>
                            <li><a href="#">С фотопечатью</a></li>
                            <li><a href="#">Звездное небо</a></li>
                            <li><a href="#">Художественные</a></li>
                        </ul>
                        <div class="footer__button-menu_check-mark"></div>
                    </div>
                    <div class="col-2 col-lg-2 footer__col_3-element footer__mobile-menu_wraper">
                        <h4 class="footer__button-menu">Услуги</h4>
                        <ul class="footer__mobile-menu_wraper-menuli">
                            <li><a href="#">Установка натяжных <br /> потолков</a></li>
                            <li><a href="#">Двухуровневые потолки</a></li>
                            <li><a href="#">Потолки с подвеской</a></li>
                            <li><a href="#">Ремонт потолков</a></li>
                        </ul>
                        <div class="footer__button-menu_check-mark"></div>
                    </div>
                    <div class="col-2 col-lg-2 footer__col_4-element footer__mobile-menu_wraper">
                        <h4 class="footer__construct footer__button-menu">Конструкции</h4>
                        <ul class="footer__mobile-menu_wraper-menuli">
                            <li><a href="#">Одноуровневые</a></li>
                            <li><a href="#">Двухуровневые</a></li>
                            <li><a href="#">Многоуровневые</a></li>
                        </ul>
                        <div class="footer__button-menu_check-mark"></div>
                    </div>
                    <div class="col-3 col-lg-3 footer__col_5-element footer__mobile-menu_wraper">
                        <div class="footer__image-bird"><a href="#"></a></div>
                        <a href="#" class="footer__ukon">ЮКОН<br><span>натяжные потолки</span></a>
                        <p></p>
                        <ul>
                            <li>
                                <address>г. Ростов-на-Дону, Шаумяна, 73</address>
                                <a class="footer__tell" href="tel:+78632298182">+7 (863) 229-81-82</a>
                            </li>
                            <li>
                                <address>г. Волгоград, Шаумяна, 73</address>
                                <a class="footer__tell" href="tel:+78632298182">+7 (863) 229-81-82</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__down">
            <div class="container">
                <div class="row">
                    <div class="col-2 col-md-4 col-lg-2 footer__col_1-element footer__mobile-menu_wraper">
                        <a href="#">ООО “ИТ-Групп”</a>
                    </div>
                    <div class="col-3 col-md-1 col-lg-2  footer__col_2-element"></div>
                    <div class="col-2 col-md-1 col-lg-3 footer__col_3-element"></div>
                    <div class="col-2 col-md-2 col-lg-2 footer__col_4-element"></div>
                    <div class="col-3 col-md-4 col-lg-3 footer__col_5-element footer__mobile-menu_wraper">
                        <ul>
                            <li class="footer__create-site"><a href="#">Создание сайта - ЕВРОСАЙТЫ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--—END footer -->

    <div id="result_form"></div>
</body>
