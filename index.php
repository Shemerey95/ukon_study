<?php
require_once ($_SERVER["DOCUMENT_ROOT"] . "/app/class/colors.class.php");
require_once ($_SERVER["DOCUMENT_ROOT"] . "/app/class/calculator.setting.class.php");
session_start();
?>

<!DOCTYPE html>
<html lang="ru">

<head>
    <link href="faviconka_ru_1133.png" rel="shortcut icon" type="image/png">
    <meta charset="UTF-8">
    <title>Ukon study</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="css/reset.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/redmond/jquery-ui-1.9.2.custom.css" />


    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <script src="js/jquery3.3.1.js" defer></script>
    <script src="js/jquery.nice-select.js" defer></script>
    <script src="js/jquery-ui-1.9.2.custom.js" defer></script>
    <script src="js/datepicker-ru.js" defer></script>
    <script src="js/jquery.maskedinput.js" defer></script>
    <script src="js/script.js" defer></script>

    <link href="css/style.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/adaptive.css">
</head>

<body>

    <!--—BEGIN HEADER -->
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-3 col-lg-2 col-md-2 header__content-left">
                    <a href="#" class="header__logo">
                        <img src="images/header-logo.png" alt="Юкон"></a>
                </div>
                <div class="col-4 col-lg-4 col-md-4 header__column_adress">
                    <address class="header__top-text-column">
                        <div><i class="fas fa-map-marker-alt"></i> г. Ростов-на-Дону, Шаумяна, 73</div>
                    </address>
                    <address class="header__text">
                        <div>г. Волгодонск, ул. Энтузиастов, 13</div>
                    </address>
                </div>
                <div class="col-2 col-lg-3 col-md-3 header__column_tel">
                    <div class="header__number-phone">
                        <a href="tel:+78632298182" class="header__top-text-column"><i class="fas fa-phone-volume"></i> +7 (863) 229-81-82</a>
                        <a href="tel:+78639247979" class="header__text">+7 (863) 924-79-79</a>
                    </div>
                </div>
                <div class="col-3 col-lg-3 col-md-3 header__column_calc">
                    <div class="header__calc-online">
                        <a href="#">
                            <div class="header__vertical">
                                <i class="fas fa-calculator"></i>
                                <div class="header__calc-text">Калькулятор<span class="header__calc-text_none"> онлайн</span></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div id="nav-container" class="header__nav-container">
            <div class="toggle-icon">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>
        </div>
        <nav id="header__mobile-menu" class="header__mobile-menu">
            <menu>
                <li><a href="#">о нас</a></li>
                <li><a href="#">каталог</a></li>
                <li><a href="#">услуги</a></li>
                <li><a href="#">цены</a></li>
                <li><a href="#">конструкции</a></li>
                <li><a href="#">акции</a></li>
                <li><a href="#">наши работы</a></li>
                <li><a href="#">контакты</a></li>
            </menu>
            <div class="header__mobile-menu_button">Наши адреса:</div>
            <ul class="header__mobile-menu_address">
                <li>
                    <address> г. Ростов-на-Дону, Шаумяна, 73</address><br>
                    <a href="tel:+78632298182">+7 (863) 229-81-82</a>
                </li>
                <li>
                    <address> г. Волгодонск, ул. Энтузиастов, 13</address><br>
                    <a href="tel:+78639247979">+7 (863) 924-79-79</a>
                </li>
            </ul>
            <div class="header__calc-online header__mobile-menu_calc">
                <a href="#">
                    <div class="vertical">
                        <i class="fas fa-calculator"></i>
                        <div class="header__calc-text">Калькулятор<p class="header__calc-text_none"> онлайн</p>
                        </div>
                    </div>
                </a>
            </div>
        </nav>
    </header>
    <!--—END HEADER -->

    <!--—BEGIN links -->
    <nav class="links">
        <div class="container">
            <menu>
                <li><a href="#">о нас</a></li>
                <li><a href="#">каталог</a></li>
                <li><a href="#">услуги</a></li>
                <li><a href="#">цены</a></li>
                <li><a href="#">конструкции</a></li>
                <li><a href="#">акции</a></li>
                <li><a href="#">наши работы</a></li>
                <li><a href="#">контакты</a></li>
            </menu>
        </div>
    </nav>
    <!--—END links -->

    <!--—BEGIN modal-window -->
    <div id="modal__window" class="modal__window">
        <span id="modal__window_close" class="modal__window_close modal__window_close-all">
            <span class="modal__window_close-wrapper modal__window_close-all">
                <span class="modal__window_line1 modal__window_close-all"></span>
                <span class="modal__window_line2 modal__window_close-all"></span>
            </span>
        </span>
        <h2>Спасибо за заявку!</h2>
        <div class="modal__window_text-wrapper">
            <p>Спасибо за Вашу заявку. Наш менеджер свяжется с Вами в ближайшее время!</p>
        </div>
    </div>
    <div id="modal__window_overlay" class="modal__window_overlay modal__window_close-all"></div>
    <!--—END modal-window -->

    <!--—BEGIN Content -->
    <div class="content">
        <form action="#" id="purchase" method="post">
            <div class="container">
                <div class="row">
                    <div class="col-8 col-lg-8 col-md-8 order-1 order-md-2 content__column_right">
                        <h3 class="content__calc-zag"><i class="fas fa-calculator"></i>Калькулятор расчета стоимости</h3>
                        <div class="row">
                            <div class="col-4 col-lg-4 col-md-4 content__right-column_first-column">
                                <h4 class="content__text-strong">характеристики:</h4>
                                <ul>
                                    <li><label for="content__square">Площадь потолка:</label></li>
                                    <li>Количество</li>
                                    <li class="content__things"><label for="content__light">светильников:</label></li>
                                    <li class="content__things"><label for="content__chandeliers">люстр:</label></li>
                                    <li class="content__things"><label for="content__pipes">труб:</label></li>
                                    <li class="content__things"><label for="content__corners">углов:</label></li>
                                </ul>
                            </div>
                            <div class="col-3 col-lg-3 col-md-4 content__col-3_text">
                                <div class="content__container_input">
                                    <input id="content__square" class="content__input-coll content__square" type="text" placeholder="0" name="square" maxlength="3" />
                                    <label for="content__square">кв.м</label>
                                </div>
                                <div class="content__container_numbers">
                                    <input id="content__light" class="content__input-coll content__square" type="text" placeholder="0" name="light" maxlength="3" />
                                    <label for="content__light">шт.</label>
                                </div>
                                <div class="content__container_numbers">
                                    <input id="content__chandeliers" class="content__input-coll content__square" type="text" placeholder="0" maxlength="3" name="chandeliers" />
                                    <label for="content__chandeliers">шт.</label>
                                </div>
                                <div class="content__container_numbers">
                                    <input id="content__pipes" class="content__input-coll content__square" type="text" placeholder="0" name="pipes" maxlength="3" />
                                    <label for="content__pipes">шт.</label>
                                </div>
                                <div class="content__container_numbers">
                                    <input id="content__corners" class="content__input-coll content__square" type="text" placeholder="0" name="corners" value="" maxlength="3" />
                                    <label for="content__corners">шт.</label>
                                </div>
                            </div>
                            <div class="col-3 col-lg-4 col-md-4 content__radio">
                                <h4 class="content__text-strong">фактура:</h4>
                                <ul>
                                    <li>
                                        <div class="content__checkbox-texture content__checkbox_flex">
                                            <input class="content__radio_input content__radio-input_top" id="content__texture_glossy" type="radio" name="texture" value="глянцевая" checked>
                                            <label for="content__texture_glossy" class="content__radio_label">глянцевая</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="content__checkbox-texture content__checkbox_flex">
                                            <input class="content__radio_input content__radio_input_top" id="content__texture_matt" type="radio" name="texture" value="матовая">
                                            <label for="content__texture_matt" class="content__radio_label">матовая</label>
                                        </div>
                                    </li>
                                    <li class="content__text-strong content__text-strong_colour">Цвет:</li>

                                    <!-- Начало вывода цветов из бд -->
                                    <?php
                                    	$colorLists = Color::getAllColors();
                                        $keys = array_keys($colorLists);
                                        $firstKey = $keys[0];
                                    ?>
									<?php
										foreach ($colorLists as $key => $value)
										{
											if ($key == $firstKey)
											{
												$check = "checked";
											} else
											{
												$check = "";
											}
									?>
										<li>
	                                        <div class="content__checkbox_colour content__checkbox_flex">
	                                            <input class="content__radio_input content__radio-input_color" id="content__colour_<?=$value?>" type="radio" name="colour" value="<?=$value?>" data-id-color="<?=$key?>" <?=$check?>>
	                                            <label for="content__colour_<?=$value?>" class="content__radio_label"><?=$value?></label>
	                                        </div>
	                                    </li>
									<?php
										}
									?>
									<!-- Конец вывода цветов из бд -->

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 col-lg-4 col-md-4 order-2 order-md-1 content__column_left">
                        <div class="content__container_left">
                            <h2 class="content__cost">итоговая цена:</h2>
                            <h2 class="content__cost content__cost-col"><span class="content__cost-total">0 рублей</span><div class="content__line"></div></h2>
                            <div class="content__main_city">
                                <label for="content__city" class="label__focus">Город доставки:</label>
                                <select id="content__city" name="city" class="focus content__city">
                                    <option value="Город не выбран" selected>Выберите город</option>
                                    <option value="Москва">Москва</option>
                                    <option value="Санкт-Петербург">Санкт-Петербург</option>
                                    <option value="Ростов-на-Дону">Ростов-на-Дону</option>
                                </select>
                            </div>
                            <div class="content__main_date-birth">
                                <label for="content__date-birth" class="label__focus">Дата рождения:</label>
                                <div class="content__main_date-birth-block">
                                    <input name="date-birth" type="text" id="content__date-birth" class="focus content__date-birth">
                                    <i id="content__datepicker-button" class="fas fa-calculator content__date-calc"></i>
                                </div>
                            </div>
                            <label for="content__tell-number" class="label__focus">Телефон</label>
                            <div class="content__bumber-block">
                                <input id="content__tell-number" type="text" placeholder="Введите номер телефона" class="focus content__tell-number" />
                            </div>
                            <button id="content__button" class="content__button" type="submit">оставить заявку</button>
                            <div class="content__agreement">
                                <input type="checkbox" id="content__agreement_checkbox" name="agreement" value="yes" class="content__agreement_checkbox" checked>
                                <label for="content__agreement_checkbox"> Я согласен на <a href="#">обработку персональных данных</a></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!--—END Content -->

    <!--—BEGIN footer -->
    <footer class="footer">
        <div class="footer__top">
            <div class="container">
                <div class="row">
                    <div class="col-2 col-lg-2 footer__col_1-element footer__mobile-menu_wraper">
                        <h4 class="footer__button-menu footer__button-menu_top-element">Компания</h4>
                        <ul class="footer__mobile-menu_wraper-menuli">
                            <li><a href="#">О нас</a></li>
                            <li><a href="#">Цены</a></li>
                            <li><a href="#">Акции</a></li>
                            <li><a href="#">Наши работы</a></li>
                            <li><a href="#">Гарантия</a></li>
                            <li><a href="#">Калькулятор</a></li>
                        </ul>
                        <div class="footer__button-menu_check-mark"></div>
                    </div>
                    <div class="col-3 col-lg-3 footer__col_2-element footer__mobile-menu_wraper">
                        <h4 class="footer__button-menu">Каталог потолков</h4>
                        <ul class="footer__mobile-menu_wraper-menuli">
                            <li><a href="#">Глянцевые</a></li>
                            <li><a href="#">Матовые</a></li>
                            <li><a href="#">Сатиновые</a></li>
                            <li><a href="#">С фотопечатью</a></li>
                            <li><a href="#">Звездное небо</a></li>
                            <li><a href="#">Художественные</a></li>
                        </ul>
                        <div class="footer__button-menu_check-mark"></div>
                    </div>
                    <div class="col-2 col-lg-2 footer__col_3-element footer__mobile-menu_wraper">
                        <h4 class="footer__button-menu">Услуги</h4>
                        <ul class="footer__mobile-menu_wraper-menuli">
                            <li><a href="#">Установка натяжных <br /> потолков</a></li>
                            <li><a href="#">Двухуровневые потолки</a></li>
                            <li><a href="#">Потолки с подвеской</a></li>
                            <li><a href="#">Ремонт потолков</a></li>
                        </ul>
                        <div class="footer__button-menu_check-mark"></div>
                    </div>
                    <div class="col-2 col-lg-2 footer__col_4-element footer__mobile-menu_wraper">
                        <h4 class="footer__construct footer__button-menu">Конструкции</h4>
                        <ul class="footer__mobile-menu_wraper-menuli">
                            <li><a href="#">Одноуровневые</a></li>
                            <li><a href="#">Двухуровневые</a></li>
                            <li><a href="#">Многоуровневые</a></li>
                        </ul>
                        <div class="footer__button-menu_check-mark"></div>
                    </div>
                    <div class="col-3 col-lg-3 footer__col_5-element footer__mobile-menu_wraper">
                        <div class="footer__image-bird"><a href="#"></a></div>
                        <a href="#" class="footer__ukon">ЮКОН<br><span>натяжные потолки</span></a>
                        <p></p>
                        <ul>
                            <li>
                                <address>г. Ростов-на-Дону, Шаумяна, 73</address>
                                <a class="footer__tell" href="tel:+78632298182">+7 (863) 229-81-82</a>
                            </li>
                            <li>
                                <address>г. Волгоград, Шаумяна, 73</address>
                                <a class="footer__tell" href="tel:+78632298182">+7 (863) 229-81-82</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__down">
            <div class="container">
                <div class="row">
                    <div class="col-2 col-md-4 col-lg-2 footer__col_1-element footer__mobile-menu_wraper">
                        <a href="#">ООО “ИТ-Групп”</a>
                    </div>
                    <div class="col-3 col-md-1 col-lg-2  footer__col_2-element"></div>
                    <div class="col-2 col-md-1 col-lg-3 footer__col_3-element"></div>
                    <div class="col-2 col-md-2 col-lg-2 footer__col_4-element"></div>
                    <div class="col-3 col-md-4 col-lg-3 footer__col_5-element footer__mobile-menu_wraper">
                        <ul>
                            <li class="footer__create-site"><a href="#">Создание сайта - ЕВРОСАЙТЫ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--—END footer -->

    <div id="result_form"></div>
</body>
</html>
