// Вызов всех функций
$(document).ready(function () {
    $('#content__city').niceSelect();
    $("#content__date-birth").datepicker();
    $('#content__tell-number').mask('+7 (999) 999-99-99');
});


// кнопка для вызова Datepicker
$('#content__datepicker-button').on('click', function () {
    $(document).ready(function () {
        $("#content__date-birth").datepicker().focus(150);
    });
    $(this).addClass("transform__main_date-birth-block");
    setTimeout(function () {
        if ($('#content__datepicker-button').hasClass("transform__main_date-birth-block")) {
            $('#content__datepicker-button').removeClass("transform__main_date-birth-block");
        }
    }, 700);
});


//  Кнопка сегодня в Datepicker
var _gotoToday = $.datepicker._gotoToday;
$.datepicker._gotoToday = function (id) {
    var target = $(id),
        inst = this._getInst(target[0]);
    if (this._get(inst, 'gotoCurrent') && inst.currentDay) {
        inst.selectedDay = inst.currentDay;
        inst.drawMonth = inst.selectedMonth = inst.currentMonth;
        inst.drawYear = inst.selectedYear = inst.currentYear;
    } else {
        var date = new Date();
        inst.selectedDay = date.getDate();
        inst.drawMonth = inst.selectedMonth = date.getMonth();
        inst.drawYear = inst.selectedYear = date.getFullYear();
    }
    this._selectDate(id, this._formatDate(inst, inst.selectedDay, inst.drawMonth, inst.drawYear));
};
const checkOffset = $.datepicker._checkOffset;
$.extend($.datepicker, {
    _checkOffset: function (inst, offset, isFixed) {
        if (isFixed) {
            return checkOffset.apply(this, arguments);
        } else {
            return offset;
        }
    }
});


//ввод только положительных цифр в поля input
$.fn.inputFilter = function (inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
        if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        }
    });
};
$(".content__input-coll").inputFilter(function (value) {
    return /^\d*$/.test(value);
});


//Скрипт для мобильного меню сайта
$(".toggle-icon").click(function () {
    $('#nav-container').toggleClass("pushed");
});
$("#nav-container").on("click", function () {
    $(this).next().slideToggle(600);
});


//Скрипт для кнопок футера
$(".footer__button-menu").on("click", function () {
    $(this).toggleClass("footer__button-menu_toggle");
    $(this).toggleClass("header__mobile-menu_button_toggle");
    $(this).next().slideToggle(600);
});


//Скрипт для кнопки меню на мобильных устройствах
$(".header__mobile-menu_button").on("click", function () {
    $(this).toggleClass("header__button-menu_toggle");
    $(this).next().slideToggle(600);
    $(this).toggleClass("header__mobile-menu_button_toggle");
});


// валидация формы и отправка данныъ на сервер
$("#content__button").on('click', function (event) {
    event.preventDefault();

    var phone = $('#content__tell-number'),
        phone_value = $(phone).val(),
        check = $("#content__agreement_checkbox"),
        check_value = $(check).prop("checked");

    $(phone).removeClass('error-box');
    $(check).removeClass('error-box');

    if (check_value == false || phone_value == "") {
        if (check_value == false) $(check).addClass('error-box');
        if (phone_value == "") $(phone).addClass('error-box');
        return;
    }

    if (phone_value.length != 18) {
        $(phone).addClass('error-boxt');
        return;
    }

    $(check).val('');
    $(phone).val('');

    //отпрака формы на сервер
    let city = $('#content__city option:selected').val(),
        dateOfBirdth = $('#content__date-birth').val(),
        squareVal = $('#content__square').val(),
        collLightVal = $('#content__light').val(),
        collChandeliersVal = $('#content__chandeliers').val(),
        collPipesVal = $('#content__pipes').val(),
        collCornersVal = $('#content__corners').val(),
        textureVal = $('.content__checkbox-texture input[name=texture]:checked').val(),
        colorVal = $('.content__checkbox_colour input[name=colour]:checked').val(),
        totalPrice = $('.content__cost-total').html(),
        totalPriceVal = parseInt(totalPrice.replace(/[^0-9]/g, ""));

    $.ajax({
        type: "POST",
        url: "../app/controllers/handler.php",
        data: {
            city:city,
            dateOfBirdth:dateOfBirdth,
            phone:phone_value,
            square:squareVal,
            collLight:collLightVal,
            collChandeliers:collChandeliersVal,
            collPipes:collPipesVal,
            collCorners:collCornersVal,
            texture:textureVal,
            color:colorVal,
            totalPrise:totalPriceVal,
            command:"sendingApplication"
        }
        }).done(function(data) {
            console.log(data);


            $('#modal__window_overlay').fadeIn(600, function () {
                $('#modal__window')
                    .css('display', 'block')
                    .animate({
                        opacity: 1,
                    top: '-6vh'
                }, 200);
            });
        });
    });


// закрытие модального окна
$('.modal__window_close-all').click(function () { // лoвим клик пo крестику или пoдлoжке
    $('#modal__window')
        .animate({
            opacity: 0,
            top: '300px'
        }, 200, function () {
            $(this).css('display', 'none'); // делaем ему display: none;
            $('#modal__window_overlay').fadeOut(400); // скрывaем пoдлoжку
        });
});

//Итоговая цена калькулятора
$(document).on("keyup click", '.content__input-coll, .content__radio-input_color, .content__checkbox-texture input', function() {
    let squareVal = $('#content__square').val(),
        collLightVal = $('#content__light').val(),
        collChandeliersVal = $('#content__chandeliers').val(),
        collPipesVal = $('#content__pipes').val(),
        collCornersVal = $('#content__corners').val(),
        textureVal = $('.content__checkbox-texture input[name=texture]:checked').val(),
        colorVal = $('.content__checkbox_colour input[name=colour]:checked').val();

        $.ajax({
            type: "POST",
            url: "../../app/controllers/handler.php",
            data: {
                square:squareVal,
                collLight:collLightVal,
                collChandeliers:collChandeliersVal,
                collPipes:collPipesVal,
                collCorners:collCornersVal,
                texture:textureVal,
                color:colorVal,
                command:"userCalculator"
            }
        }).done(function(data) {
            $('.content__cost-total').text(data + " рублей");
        });
})
