<?php
    require_once($_SERVER["DOCUMENT_ROOT"] . "/app/class/db.class.php");
    require_once($_SERVER["DOCUMENT_ROOT"] . "/app/class/users.class.php");
    require_once($_SERVER["DOCUMENT_ROOT"] . "/app/class/colors.class.php");
    require_once($_SERVER["DOCUMENT_ROOT"] . "/app/class/calculator.setting.class.php");

	function clearData($data)
	{
		return trim(htmlspecialchars($data));
	}

	$command = clearData($_POST['command']);

	switch ($command) {

        // Авторизация пользователя
        case 'autorization':
            $login = clearData($_POST['loginUser']);
            $password = clearData($_POST['passwordUser']);
            User::Autorization($login, $password);
            break;

		// Добавление пользователя
		case 'addUser':
			$login = clearData($_POST['usLogin']);
			$password = clearData($_POST['usPassword']);
			User::AddUser($login, $password);
		break;

		// удаление пользователя
		case 'deleteUser':
			$ldUserDelete = clearData($_POST['usIdDelete']);
			User::DeleteUser($ldUserDelete);
		break;

		// Изменение информации о пользователе
		case 'editUser':
			$editLogin = clearData($_POST['editLogin']);
			$editPassword = clearData($_POST['editPassword']);
			$editIdUser = clearData($_POST['editId']);
			User::EditUser($editIdUser, $editLogin, $editPassword);
			break;

        // Добавление цвета
        case 'addColor':
            $colorName = clearData($_POST['colorName']);
            Color::addColor($colorName);
            break;

        // Изменение настроек калькулятора
        case 'settingCalculator':
            $costTextureMatt = clearData($_POST['textureMatt']);
            $costPipes = clearData($_POST['pipes']);
            $costTextureGlossy = clearData($_POST['textureGlossy']);
            $costChandeliers = clearData($_POST['chandeliers']);
            $costSquare = clearData($_POST['square']);
            $costLight = clearData($_POST['light']);
            $costCorner = clearData($_POST['corner']);
            CalculatorSettings::editCalculatorSettings($costTextureMatt, $costPipes, $costTextureGlossy, $costChandeliers, $costSquare, $costLight, $costCorner);
            break;

        // удаление цвета
        case 'deleteColor':
            $ldColorDelete = clearData($_POST['colorIdDelete']);
            Color::deleteColor($ldColorDelete);
        break;

        // Расчет итоговой цены для пользователя на публичной странице
        case 'userCalculator':
            $square = clearData($_POST['square']);
            $collLight = clearData($_POST['collLight']);
            $collChandeliers = clearData($_POST['collChandeliers']);
            $collPipes = clearData($_POST['collPipes']);
            $collCorners = clearData($_POST['collCorners']);
            $texture = clearData($_POST['texture']);
            CalculatorSettings::totalPrice($square, $collLight, $collChandeliers, $collPipes, $collCorners, $texture);
            break;

        // Отправка заявки на сервер
        case 'sendingApplication':
            $city = clearData($_POST['city']);
            $dateOfBirdth = clearData($_POST['dateOfBirdth']);
            $phone = clearData($_POST['phone']);
            $square = clearData($_POST['square']);
            $collLight = clearData($_POST['collLight']);
            $collChandeliers = clearData($_POST['collChandeliers']);
            $collPipes = clearData($_POST['collPipes']);
            $collCorners = clearData($_POST['collCorners']);
            $texture = clearData($_POST['texture']);
            $color = clearData($_POST['color']);
            $totalPrise = clearData($_POST['totalPrise']);
		    User::addRequest($city, $dateOfBirdth, $phone, $square, $collLight, $collChandeliers, $collPipes, $collCorners, $texture, $color, $totalPrise);

        // Удаление заявок в бд
        case 'deleteRequest':
            $idRequest = clearData($_POST['requestIdDelete']);
            User::deleteRequest($idRequest);
	}
