<?php
require_once ($_SERVER["DOCUMENT_ROOT"] . "/app/class/db.class.php");
/**
 * @var string Color класс для работы с цветами
 */
Class Color
{
	/**
     * Вывод цветов на административную страницу из БД
     * @return $colorsDb массив цветов
     */
	public function getAllColors()
	{
		$connect = DB::getInstance();
		$query = "SELECT `colors` FROM `settings` WHERE `id_setting` = '1'";
		$result = $connect->query($query);
		if ($result->num_rows > 0)
		{
			$colors = $result->fetch_assoc();
			$colorsDb = json_decode($colors['colors'], true);
			return $colorsDb;
		} else
		{
			echo "Не найдено ни одного цвета!";
		}
	}

    /**
     * Добавление цвета в БД
     * @param string $colorName название цвета
     */
	static function addColor($colorName)
	{
		$connect = DB::getInstance();

		$jsonСonclusion = array();
		$jsonDb = array();
		$colorFlag = false;
		$jsonСonclusion['flag'] = false;

		$query = "SELECT `colors` FROM `settings` WHERE `id_setting` = '1'";
		$result = $connect->query($query);
		$color = $result->fetch_assoc();

		$jsonDb = json_decode($color['colors'], true);
		if ($jsonDb == [])
		{
			$jsonDb['1'] = $colorName;
			$colorFlag = true;
			$jsonСonclusion['flag'] = true;
			$jsonСonclusion['exists'] = 'Цвет успешно добавлен!';
		} else
		{
			if (in_array("$colorName", $jsonDb))
			{
				$jsonСonclusion['exists'] = 'Данный цвет уже существует! Введите другой.';
			} else
			{
				array_push($jsonDb, $colorName);
				$colorFlag = true;
				$jsonСonclusion['flag'] = true;
				$jsonСonclusion['exists'] = 'Цвет успешно добавлен!';
			}
		}

		$keyElement = array_search($colorName, $jsonDb);

		$jsonToDb = json_encode($jsonDb, JSON_UNESCAPED_UNICODE);
		$sql = "UPDATE `settings` SET `colors`='".$jsonToDb."'";
		$result = $connect->query($sql);
		if ($colorFlag)
		{
			$jsonСonclusion['html'] = "<div class='content__calc-setting_color-options' data-id-color='".$keyElement."'>
                                    		".$colorName."
                                    		<a href='#' class='content__calc-setting_delete' data-id-delete='".$keyElement."'>×</a>
                                	   </div>";
		}
		echo json_encode($jsonСonclusion, JSON_UNESCAPED_UNICODE);
	}

	/**
     * Удаление цвета из БД
     * @param integer $ldColorDelete идентификатор цвета
     */
	static function deleteColor($ldColorDelete)
	{
		$connect = DB::getInstance();
		$jsonDb = array();

		$query = "SELECT `colors` FROM `settings` WHERE `id_setting` = '1'";
		$resultColorList = $connect->query($query);
		$color = $resultColorList->fetch_assoc();
		$jsonDb = json_decode($color['colors'], true);

		unset($jsonDb[$ldColorDelete]);

		$jsonToDb = json_encode($jsonDb, JSON_UNESCAPED_UNICODE);
		$sql = "UPDATE `settings` SET `colors`='".$jsonToDb."'";
		$resultUpdate = $connect->query($sql);
	}
}