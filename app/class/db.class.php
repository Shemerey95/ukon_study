<?php
/**
 * @var string DB класс для подключения к БД
 */
class DB {

    /**
     * Создание подключение к БД Singletone
     * @return self::$instance подключение к БД
     */
    protected $db;
    static private $instance = null;

    private function __construct()
    {
    	$paramsPath = ($_SERVER["DOCUMENT_ROOT"] . "../app/config/db_params.php");
    	$params = require_once($paramsPath);

        $this->db = new mysqli($params['host'], $params['user'], $params['password'], $params['dbname']
		);
    }

    private function __clone() {}

    public static function getInstance() {
    if(self::$instance == null)
    {
        self::$instance = new self();
    }
    return self::$instance;
    }

    public function query($sql)
    {
        return $this->db->query($sql);
    }
}
