<?php
require_once ($_SERVER["DOCUMENT_ROOT"] . "/app/class/db.class.php");

/**
 * @var string User класс для работы с пользователями и их данными
 */
class User
{
    /**
     * Авторизация пользователя на сайте
     * @param string $login имя пользователя
     * @param stirng $password пароль пользователя
     */
    static function Autorization($login, $password)
    {
        $connect = DB::getInstance();
        $json['flag'] = false;
        $json['invalid'] = '';

        $sql = "SELECT * FROM `users` WHERE `login` = '".$login."'";
        $result = $connect->query($sql);
        if ($result->num_rows > 0)
        {
            $user = $result->fetch_assoc();
            if ($user['passwd'] == md5($password))
            {
                $json['flag'] = true;
                session_start();
                $_SESSION['userLoged'] = 'Loged';
                // setcookie('userLoged', 'Loged', time() + 2000, "/");
            } else
            {
                $json['invalid'] = 'Неверный пароль!';
            }
        } else
        {
            $json['invalid'] = 'Пользователей не найдено!';
        }
        echo json_encode($json);
    }

    /**
     * Вывод информации о пользователях из БД
     * @return $users массив пользователей из БД
     */
    public function GetAllUsers()
    {
        $users = array();
        $connect = DB::getInstance();

        $sql = "SELECT * FROM `users`";
        $result = $connect->query($sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
                {
                    $users[] = $row;
                }
        } else {
            echo 'Пользователей не найдено!';
        }

        return $users;
    }

    /**
     * Добавление нового пользователя в БД
     * @param string $login имя пользователя
     * @param stirng $password пароль пользователя
     */
    static function AddUser($login, $password)
    {
    	$connect = DB::getInstance();

		$json = array();
    	$query = "SELECT * FROM `users` WHERE `login` = '" . $login . "'";
		$resultListUser = $connect->query($query);
		if ($resultListUser->num_rows > 0)
		{
			$json["alreadyExists"] = "Такой пользователь уже существует!";
		} else
		{
			$sql = "INSERT INTO `users` (`login`, `passwd`) VALUES ('".$login."', '".md5($password)."')";
	    	$resultToDb = $connect->query($sql);

	    	$sqll = "SELECT `id_user` FROM `users` WHERE `login` = '".$login."'";
	    	$resultIdLastUser = $connect->query($sqll);
	    	$idLoginUser = $resultIdLastUser->fetch_assoc();

	    	if ($resultToDb)
	    	{
    		$json["exist"] = "Пользователь успешно добавлен!";

            $json["html"] = "
                            <tr>
                                <td>".$idLoginUser['id_user']."</td>
                                <td id='userLogin".$idLoginUser['id_user']."'>".$login."</td>
                                <td class='content__table-users_last-column'>
                                    <a href='#' class='content__table-users_edit' data-id='".$idLoginUser['id_user']."' data-login-user='".$login."'></a>
                                    <a href='#' class='content__table-users_delete' data-id='".$idLoginUser['id_user']."'>&#215;</a>
                                </td>
                            </tr>";
			}
		}
		echo json_encode($json);
    }

    /**
     * Удаление пользователей из БД
     * @param integer $ldUserDelete идентификатор пользователя
     */
    static function DeleteUser($ldUserDelete)
    {
    	$connect = DB::getInstance();
    	$sql = "DELETE FROM `users` WHERE `id_user` = $ldUserDelete";
    	$result = $connect->query($sql);
    	if ($result) echo 'Пользователь успешно удалён!';
    }

    /**
     * Изменение информации о пользователе в БД
     * @param integer $editIdUser идентификатор пользователя
     * @param string $editLogin логин пользователя
     * @param stirng $editPassword пароль пользователя
     */
    static function EditUser($editIdUser, $editLogin, $editPassword)
    {
    	$json = array();
        $json['bool'] = false;
    	$connect = DB::getInstance();

    	$query = "SELECT * FROM `users` WHERE `login` = '".$editLogin."'";
		$users = $connect->query($query);
		if ($result->num_rows > 0)
		{
			$json['alreadyExists'] = "Такой пользователь уже существует!";

		} else
		{
            if ($editPassword == '')
            {
                $sql = "UPDATE `users` SET `login` = '".$editLogin."' WHERE `users`.`id_user` = '".$editIdUser."'";
                $resultLogin = $connect->query($sql);
                if ($resultLogin)
                {
                    $json['bool'] = true;
                    $json['successfully'] = 'Информация успешно изменена!';
                }
            } else
            {
			$sql = "UPDATE `users` SET `login` = '".$editLogin."', `passwd` = '".md5($editPassword)."' WHERE `users`.`id_user` = '".$editIdUser."'";
    		$resultUpdateUser = $connect->query($sql);
        		if ($resultUpdateUser)
                {
                    $json['successfully'] = 'Информация успешно изменена!';
                    $json['bool'] = true;
                }
            }
		}
        echo json_encode($json);
    }

    /**
     * Создание текста заявкок для таблицы заявок
     * @param integer $square площадь потолка
     * @param integer $collLight колличество светильников
     * @param integer $collChandeliers колличество люстр
     * @param integer $collPipes колличество труб
     * @param integer $collCorners колличество углов
     * @param string $texture текстура
     * @param stirng $color цвет
     * @param integer $totalPrise итоговая цена
     * @return $textRequest сформированный текст заявки
     */
    private static function createTextRequest($square, $collLight, $collChandeliers, $collPipes, $collCorners, $texture, $color, $totalPrise)
    {
        $textRequest = "";

        if ($square)
            $textRequest .='Площадь потолка: ' . $square . ' кв.м.;<br/>';
        if ($collLight)
            $textRequest .='Светильников: ' . $collLight . ' шт.;<br/>';
        if ($collChandeliers)
            $textRequest .='Люстр: ' . $collChandeliers . ' шт.;<br/>';
        if ($collPipes)
            $textRequest .='Труб: ' . $collPipes . ' шт.;<br/>';
        if ($collCorners)
            $textRequest .='Углов: ' . $collCorners . ' шт.;<br/>';
        if ($texture)
            $textRequest .='Фактура: ' . $texture . ';<br/>';
        if ($color)
            $textRequest .='Цвет: ' . $color . ';<br/>';
        if ($totalPrise)
            $textRequest .='Итоговая сумма: ' . $totalPrise . ' рублей';

        return $textRequest;
    }

    /**
     * Формирование текста заявки для отправки на почту администратора
     * @param integer $square площадь потолка
     * @param integer $collLight колличество светильников
     * @param integer $collChandeliers колличество люстр
     * @param integer $collPipes колличество труб
     * @param integer $collCorners колличество углов
     * @param string $texture текстура
     * @param stirng $color цвет
     * @param integer $totalPrise итоговая цена
     * @return $textRequestToEmail сформированный текст заявки для отправки на почту
     */
    private static function createTextRequestToEmail($square, $collLight, $collChandeliers, $collPipes, $collCorners, $texture, $color, $totalPrise)
    {
        $textRequestToEmail = "";

        if ($square)
            $textRequestToEmail .='Площадь потолка: ' . $square . ' кв.м.
';
        if ($collLight)
            $textRequestToEmail .='Количество ветильников: ' . $collLight . ' шт.
';
        if ($collChandeliers)
            $textRequestToEmail .='Количество люстр: ' . $collChandeliers . ' шт.
';
        if ($collPipes)
            $textRequestToEmail .='Количество труб: ' . $collPipes . ' шт.
';
        if ($collCorners)
            $textRequestToEmail .='Количество углов: ' . $collCorners . ' шт.
';
        if ($texture)
            $$textRequestToEmail .='Фактура: ' . $texture . '
';
        if ($color)
            $textRequestToEmail .='Цвет: ' . $color . '
';
        if ($totalPrise)
            $textRequestToEmail .='Итоговая сумма: ' . $totalPrise . ' рублей';

        return $textRequestToEmail;
    }

    /**
     * отправка заявки в БД и на почту администратору
     * @param string $city город доставки
     * @param string $dateOfBirdth дата рождения
     * @param string $phone телефон
     * @param integer $square площадь потолка
     * @param integer $collLight колличество светильников
     * @param integer $collChandeliers колличество люстр
     * @param integer $collPipes колличество труб
     * @param integer $collCorners колличество углов
     * @param string $texture текстура
     * @param stirng $color цвет
     * @param integer $totalPrise итоговая цена
     */
    static function addRequest($city, $dateOfBirdth, $phone, $square, $collLight, $collChandeliers, $collPipes, $collCorners, $texture, $color, $totalPrise)
    {
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = @$_SERVER['REMOTE_ADDR'];
        if(filter_var($client, FILTER_VALIDATE_IP)) $ip = $client;
        elseif(filter_var($forward, FILTER_VALIDATE_IP)) $ip = $forward;
        else $ip = $remote;
        echo $ip;

        if ($dateOfBirdth == "") $dateOfBirdth = "Дата рождения не выбрана";

        $textRequest = self::createTextRequest($square, $collLight, $collChandeliers, $collPipes, $collCorners, $texture, $color, $totalPrise);

        $textRequestToEmail = self::createTextRequestToEmail($square, $collLight, $collChandeliers, $collPipes, $collCorners, $texture, $color, $totalPrise);

        $applicationDate = date('H:i d.m.Y');

        $connect = DB::getInstance();
        $sql = "INSERT INTO `request` (`delivery_city`, `date_of_birth`, `phone`, `text_request`, `application_date`, `ip_user`) VALUES ('".$city."', '".$dateOfBirdth."', '".$phone."', '".$textRequest."', '".$applicationDate."', '".$ip."')";
        print_r($sql);
        $result = $connect->query($sql);



        mail('test@mail.ru', 'Subject', "Город доставки: $city
Дата рождения: $dateOfBirdth
Телефон: $phone
$textRequestToEmail
Дата заявки: $applicationDate
IP заказчика: $ip.");
    }

    /**
     * Вывод всех заказов на административную страницу из БД
     * @return $requests stirng массив заявок из БД
     */
    public static function getAllRequest()
    {
        $connect = DB::getInstance();
        $sql = "SELECT * FROM `request`";
        $result = $connect->query($sql);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
                {
                    $requests[] = $row;
                }
        } else {
            echo 'Заказов нет!';
        }
        return $requests;
    }

    /**
     * Удаление заявок пользователей в БД
     * @param integer $idRequest идентификатор заявки
     */
    static function deleteRequest($idRequest)
    {
        $connect = DB::getInstance();
        $sql = "DELETE FROM `request` WHERE `id_request` = ".$idRequest."";
        $result = $connect->query($sql);
        if ($result) echo 'Заявка успешно удалена!';
    }
}
