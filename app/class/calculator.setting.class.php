<?php
require_once ($_SERVER["DOCUMENT_ROOT"] . "/app/class/db.class.php");
/**
 * @var string CalculatorSettings класс для работы с настройками калькулятора
 */
Class CalculatorSettings
{

	/**
	 * Вывод настроек калькулятора из БД на административную страницу
	 * @return $settings массив значений настроек калькулятора расчета итоговой стоимости заказа
	 */
	static function getCalcSetting()
	{
		$connect = DB::getInstance();
		$sql = "SELECT * FROM `settings`";
		$result = $connect->query($sql);
		$settings = $result->fetch_assoc();

		return $settings;
	}

	/**
     * Изменение настроек калькулятора в БД
     * @param integer $costTextureMatt стоимость матовой текстуры
     * @param integer $costPipes стоимость трубы
     * @param integer $costTextureGlossy стоимость глянцевой фактуры
     * @param integer $costChandeliers стоимость люстры
     * @param integer $costSquare стоимость квадратного метра потолка
     * @param integer $costLight стоимость светильника
     * @param integer $costCorner стоимость угла
     */
	static function editCalculatorSettings($costTextureMatt, $costPipes, $costTextureGlossy, $costChandeliers, $costSquare, $costLight, $costCorner)
	{
		$connect = DB::getInstance();
		$sql = "UPDATE `settings` SET `id_setting`='1',`cost_square`= '".$costSquare."',`cost_light`='".$costLight."',`cost_chandelier`='".$costChandeliers."',`cost_pipe`='".$costPipes."',`cost_corner`='".$costCorner."',`cost_glossy_texture`='".$costTextureGlossy."',`cost_matt_texture`='".$costTextureMatt."'";
		$result = $connect->query($sql);
		if ($result) echo "Настройки успешно сохранены!";
	}

	/**
     * Вывод итоговой цены для заказчика на публичную страницу
     * @param integer $square площадь потолка
     * @param integer $collLight колличество светильников
     * @param integer $collChandeliers колличество люстр
     * @param integer $collPipes колличество труб
     * @param integer $collCorners колличество углов
     * @param string $texture текстура
     */
	static function totalPrice($square, $collLight, $collChandeliers, $collPipes, $collCorners, $texture)
	{
		$connect = DB::getInstance();
		$sql = "SELECT * FROM `settings` WHERE `id_setting` = 1";
		$result = $connect->query($sql);
		$costAndColl = $result->fetch_assoc();

		$textureVal = $costAndColl['cost_glossy_texture'];
		if ($texture == 'матовая') {
			$textureVal =  $costAndColl['cost_matt_texture'];
		};

		$totalPrice = ($costAndColl['cost_square'] * $square * $textureVal) + ($collLight * $costAndColl['cost_light']) + ($collChandeliers * $costAndColl['cost_chandelier']) + ($collPipes * $costAndColl['cost_pipe']) + ($collCorners * $costAndColl['cost_corner']);
		echo $totalPrice;
	}
}