<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "../app/class/users.class.php");
?>

<!DOCTYPE html>
<html lang="ru">

<head>
    <link href="faviconka_ru_1133.png" rel="shortcut icon" type="image/png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="css/reset.css">
    <script src="js/jquery3.3.1.js" defer></script>
    <script src="js/script.js" defer></script>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/adaptive.css">
    <title>Авторизацмя</title>
</head>

<body>
    <section class="body__authorization">
        <form method="post" action="#" id="body__authorization" class="body__authorization_form">
            <h2>Авторизация</h2>
            <p>Для входа введите логин и пароль</p>
            <label for="body__authorization_login">Логин:</label>
            <input type="text" name="username" id="body__authorization_login" class="body__authorization_login" placeholder="Ваш логин">
            <label for="body__authorization_passwd">Пароль:</label>
            <input type="password" name="passwd" id="body__authorization_passwd" class="body__authorization_passwd" placeholder="Ваш пароль">
            <span class="body__text-autorization"><span></span></span>
            <button type="submit" id="body__authorization_submit" class="body__authorization_submit">войти</button>
        </form>
    </section>
</body>

</html>


