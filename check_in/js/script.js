// валидация формы и авторизация пользователя
$("#body__authorization_submit").on('click', function (event) {
    event.preventDefault();

    var login = $('#body__authorization_login'),
        login_value = $(login).val(),
        passwd = $('#body__authorization_passwd'),
        passwd_value = $(passwd).val();

    $(login).removeClass('error-box');
    $(passwd).removeClass('error-box');

    if (login_value == "" || passwd_value == "") {
        if (login_value == "") $(login).addClass('error-box');
        if (passwd_value == "") $(passwd).addClass('error-box');
        return;
    }
    
    if (passwd_value.length < 5) {
        $(passwd).addClass('error-box');
        $('.body__text-autorization span').text('Минимальная длина пароля 5 символов!');
        return;
    }

    else {
         $.ajax({
                type: "POST",
                url: "../../app/controllers/handler.php",
                data: {
                    loginUser:login_value,
                    passwordUser:passwd_value,
                    command:"autorization"
                }
                }).done(function(data) {
                    let user = JSON.parse(data);
                        console.log(user.invalid);
                        $('.body__text-autorization span').text(user.invalid).fadeIn(0);
                        $('.body__text-autorization span').text(user.invalid).delay(2000).fadeOut(600);
                        if (user.flag == true) window.location.replace("../../administration/index.php");
                });
    }
    $(login).val('');
    $(passwd).val('');
});